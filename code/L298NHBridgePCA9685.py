#!/usr/bin/env python
# coding: latin-1
# Autor:   Ingmar Stapel
# Datum:   20160731
# Version:   2.0
# Homepage:   http://custom-build-robots.com

#This program was developed for the control of the 
#left and right motors of the robot car. It assumes that a L298N H-bridge is used as motor driver.

#This program must be called from a parent program that takes control of the L298NHBridge program.

#The class RPi.GPIO is imported, which enables the control of the GPIO pins of the Raspberry Pi.

from __future__ import division
import RPi.GPIO as io
io.setmode(io.BCM)

import time

# Import Adafruit PCA9685 library
import Adafruit_PCA9685
# Initialise the PCA9685 using the default address (0x40).
PCA9685_pwm = Adafruit_PCA9685.PCA9685(address=0x43)

# Set frequency to 100hz, good for l298n h-bridge.
PCA9685_pwm.set_pwm_freq(60)
# The variable duty_cycleMax specifies the maximum duty cycle of the motors per 
# 100 herts. For the speed of the motors the duty cycle always starts at 0 and 
# ends at a value ]0, 4095[.
duty_cycleMax = 4095

# The following call deactivates any warnings that the 
# class RPi.GPIO will be disabled.
io.setwarnings(False)

# In the following program section, the logical wiring of the Raspberry 
# Pi is mapped in the program. For this purpose the pins known from the 
# motor driver are assigned the GPIO addresses.
# --- START KONFIGURATION GPIO Addresses ---
##ENA = 20
leftmotor_in1_pin = 6
leftmotor_in2_pin = 13
rightmotor_in1_pin = 19
rightmotor_in2_pin = 26
##ENB = 21
# --- END KONFIGURATION GPIO Addresses ---

# Both variables leftmotor_in1_pin and leftmotor_in2_pin are defined as 
# outputs "OUT". With the two variables the direction of rotation of the 
# motors.io.setup(leftmotor_in1_pin, io.OUT)
io.setup(leftmotor_in2_pin, io.OUT)
io.setup(rightmotor_in1_pin, io.OUT)
io.setup(rightmotor_in2_pin, io.OUT)
 
# Both variables rightmotor_in1_pin and rightmotor_in2_pin are defined as 
# outputs "OUT". With the two variables the direction of rotation of the 
# motors is controlled.

# The GPIO pins of the Raspberry Pi are initially set to False. This ensures 
# that there is no HIGH signal and the # motor driver is not activated unintentionally.  
# motor driver is not activated unintentionally.io.output(leftmotor_in1_pin, False)
io.output(leftmotor_in2_pin, False)
io.output(rightmotor_in1_pin, False)
io.output(rightmotor_in2_pin, False)

# The setMotorMode(motor, mode) function sets the direction of rotation of the motors. 
# The function has two input variables.
# motor -> this variable determines whether the left or right # motor is selected. 
# motor is selected.
# mode -> this variable determines which mode is selected.
def setMotorMode(motor, mode):
   if motor == "leftmotor":
      if mode == "reverse":
         io.output(leftmotor_in1_pin, True)
         io.output(leftmotor_in2_pin, False)
      elif  mode == "forward":
         io.output(leftmotor_in1_pin, False)
         io.output(leftmotor_in2_pin, True)
      else:
         io.output(leftmotor_in1_pin, False)
         io.output(leftmotor_in2_pin, False)
   elif motor == "rightmotor":
      if mode == "reverse":
         io.output(rightmotor_in1_pin, False)
         io.output(rightmotor_in2_pin, True)      
      elif  mode == "forward":
         io.output(rightmotor_in1_pin, True)
         io.output(rightmotor_in2_pin, False)
      else:
         io.output(rightmotor_in1_pin, False)
         io.output(rightmotor_in2_pin, False)
   else:
      io.output(leftmotor_in1_pin, False)
      io.output(leftmotor_in2_pin, False)
      io.output(rightmotor_in1_pin, False)
      io.output(rightmotor_in2_pin, False)

# The function setMotorLeft(power) sets the speed of the left motors. The speed is 
# passed as a value between -1 and 1. With a negative value the motors should turn 
# backwards, otherwise forward. Afterwards the necessary # %-values for the PWM are 
# calculated from the given values. %-values for the PWM signal are calculated.
def pwmDutyCycle(input):
   return duty_cycleMax if pwm > duty_cycleMax else input

def setMotorLeft(power):
   int(power)
   if power < 0:
      # Reverse mode for the left motor
      setMotorMode("leftmotor", "reverse")
      pwm = pwmDutyCycle(-int(duty_cycleMax * power))
   elif power > 0:
      # Forward mode for the left motor
      setMotorMode("leftmotor", "forward")
      pwm = pwmDutyCycle(int(duty_cycleMax * power))
   else:
      # Stop mode for the left motor
      setMotorMode("leftmotor", "stopp")
      pwm = 0
   PCA9685_pwm.set_pwm(0, 0, pwm)
# The function setMotorRight(power) sets the speed of the right motors. The speed 
# is passed as a value between -1 and 1. With a negative value the motors should 
# turn backwards, otherwise forward. Afterwards the necessary # %-values for the 
# PWM are calculated from the given values.  %-values for the PWM signal are calculated. 
def setMotorRight(power):
   int(power)
   if power < 0:
      # Reverse mode for the right motor
      setMotorMode("rightmotor", "reverse")
      pwm = pwmDutyCycle(-int(duty_cycleMax * power))
   elif power > 0:
      # Forward mode for the right motor
      setMotorMode("rightmotor", "forward")
      pwm = pwmDutyCycle(int(duty_cycleMax*power))
   else:
      # Stop mode for the right motor
      setMotorMode("rightmotor", "stopp")
      pwm = 0
   ##rightmotorpwm.ChangeDutyCycle(pwm)
   PCA9685_pwm.set_pwm(1, 0, pwm)
# The exit() function sets the outputs that control the motor driver to false. 
# So after calling the function the motor driver is in a saved state and the 
# motors are are stopped.def exit():
   io.output(leftmotor_in1_pin, False)
   io.output(leftmotor_in2_pin, False)
   io.output(rightmotor_in1_pin, False)
   io.output(rightmotor_in2_pin, False)
   io.cleanup()

def setServoY(deg):
   PCA9685_pwm.set_pwm(14, 0, int(duty_cycleMax * ((0.002/180) * (int(deg) + 90) + 0.0005) * 60))
    
def setServoX(deg):
   PCA9685_pwm.set_pwm(15, 0, int(duty_cycleMax * ((0.002 / 180) * (int(deg) + 90) + 0.0005) * 60))
