import sys, tty, termios, os, readchar
# Das Programm L298NHBridge.py wird als Modul geladen. Es stellt
# die Funktionen fuer die Steuerung der H-Bruecke zur Verfuegung.
import L298NHBridgePCA9685 as HBridge

# Variablen Definition der linken und rechten Geschwindigkeit der
# Motoren des Roboter-Autos.
speedleft = 0
speedright = 0
servox = 0
servoy = 0
speedCap = 90
servoSpeedCap = 0.2
# Das Menue fuer den Anwender wenn er das Programm ausfuehrt.
# Das Menue erklaert mit welchen Tasten das Auto gesteuert wird.
print("w/s: beschleunigen\na/d: lenken\ni/j/k/l: Kamera lenken\nq: stoppt die Motoren\nx: Programm beenden")
# Die Funktion printscreen() gibt immer das aktuelle Menue aus
# sowie die Geschwindigkeit der linken und rechten Motoren wenn
# es aufgerufen wird.
def speedcapUp(speed):
   return servoSpeedCap if speed > servoSpeedCap else speed

def speedcapDown(speed):
   return -servoSpeedCap if speed < -servoSpeedCap else speed

def servoSpeedCapUp(speed):
   return speedCap if speed > speedCap else speed

def servoSpeedCapDown(speed):
   return -speedCap if speed < -speedCap else speed

def printscreen():
   # der Befehl os.system('clear') leert den Bildschirmihalt vor
   # jeder Aktualisierung der Anzeige. So bleibt das Menue stehen
   # und die Bildschirmanzeige im Terminal Fenster steht still.
   os.system('clear')
   print(
      "w/s: beschleunigen\na/d: lenken\ni/j/k/l: Kamera lenken\nq:   stoppt die Motoren\nx:   Programm beenden",
      "========== Geschwindigkeitsanzeige ==========\nGeschwindigkeit linker Motor:  ", 
      speedleft,
      "\nGeschwindigkeit rechter Motor: ", 
      speedright
      )

# Diese Endlosschleife wird erst dann beendet wenn der Anwender 
# die Taste X tippt. Solange das Programm laeuft wird ueber diese
# Schleife die Eingabe der Tastatur eingelesen.
while True:
   # Mit dem Aufruf der Funktion getch() wird die Tastatureingabe 
   # des Anwenders eingelesen. Die Funktion getch() liesst den 
   # gedrueckte Buchstabe ein und uebergibt diesen an die 
   # Variablechar. So kann mit der Variable char weiter 
   # gearbeitet werden.
   char = readchar.readchar()
# mit dem druecken der Taste "q" werden die Motoren angehalten
  if(char == "q"):
    speedleft = 0
    speedright = 0
    HBridge.setMotorLeft(speedleft)
    HBridge.setMotorRight(speedright)
    printscreen()  
  # Das Roboter-Auto faehrt vorwaerts wenn der Anwender die 
  # Taste "w" drueckt.
  if(char == "w"):
  # das Roboter-Auto beschleunigt in Schritten von 10% 
  # mit jedem Tastendruck des Buchstaben "w" bis maximal 
  # 100%. Dann faehrt es maximal schnell vorwaerts.
  # Dem Programm L298NHBridge welches zu beginn  
  # importiert wurde wird die Geschwindigkeit fuer 
  # die linken und rechten Motoren uebergeben.
    speedleft = speedcapUp(speedleft + 0.1)
    speedright = speedcapUp(speedright + 0.1)
    HBridge.setMotorLeft(speedleft)
    HBridge.setMotorRight(speedright)
    printscreen()

   # Das Roboter-Auto faehrt rueckwaerts wenn die Taste "s" 
   # gedrueckt wird.
  if(char == "s"):
  # das Roboter-Auto bremst in Schritten von 10% 
  # mit jedem Tastendruck des Buchstaben "s" bis maximal 
  # -100%. Dann faehrt es maximal schnell rueckwaerts.
  # Dem Programm L298NHBridge welches zu beginn  
  # importiert wurde wird die Geschwindigkeit fuer 
  # die linken und rechten Motoren uebergeben.      
    speedleft = speedcapDown(speedleft - 0.1)
    speedright = speedcapDown(speedright - 0.1)         
    HBridge.setMotorLeft(speedleft)
    HBridge.setMotorRight(speedright)
    printscreen()

   # Mit der Taste "d" lenkt das Auto nach rechts bis die max/min
   # Geschwindigkeit der linken und rechten Motoren erreicht ist.
  if(char == "d"):      
    speedright = speedcapDown(speedright - 0.1)
    speedleft = speedcapUp(speedleft + 0.1)
    HBridge.setMotorLeft(speedleft)
    HBridge.setMotorRight(speedright)
    printscreen()
      
   # Mit der Taste "a" lenkt das Auto nach links bis die max/min
   # Geschwindigkeit der linken und rechten Motoren erreicht ist.
  if(char == "a"):
    speedleft = speedcapDown(speedleft - 0.1)
    speedright = speedcapUp(speedright + 0.1)
    HBridge.setMotorLeft(speedleft)
    HBridge.setMotorRight(speedright)
    printscreen()

  if(char == "i"):
    servoy = servoSpeedCapUp(servoy + 1)
    HBridge.setServoY(servoy)
    printscreen()

  if(char == "k"):
    servoy = servoSpeedCapDown(servoy - 1)
    HBridge.setServoY(servoy)
    printscreen()

  if(char == "j"):
    servox = servoSpeedCapUp(servox + 1)
    HBridge.setServoX(servox)
    printscreen()

  if(char == "l"):
    servox = servoSpeedCapDown(servox - 1)
    HBridge.setServoX(servox)
    printscreen()


   # Mit der Taste "x" wird die Endlosschleife beendet 
   # und das Programm wird ebenfalls beendet. Zum Schluss wird 
   # noch die Funktion exit() aufgerufen die die Motoren stoppt.
  if(char == "x"):
    HBridge.setMotorLeft(0)
    HBridge.setMotorRight(0)
    HBridge.exit()
    print("Program shuddown")
    break
   
  print(char)
  # Die Variable char wird pro Schleifendurchlauf geleert. 
  # Das ist notwendig um weitere Eingaben sauber zu ubernehmen.
  char = ""
#ende
